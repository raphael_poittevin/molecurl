# README #

Prerequisites :

- You will need to have a local MongoDB running on port 27017
- The Angular part is not working yet :/

### How to build ###

In the root folder of the project, simply run :

> gradle bootRun

This will deploy a local server using tomcat an provide you with the webservice on port 8080.

### How to use ###
Going to localhost with the parameter url used should give you something like that :

> http://localhost:8080/encode?url=http%3A%2F%2Fs2.quickmeme.com%2Fimg%2Fed%2Feddd639ecae8a1466b5cb7ca38e47f20f48d3bb2b47439be5fba6919b90c3429.jpg

> The short version of 'http://s2.quickmeme.com/img/ed/eddd639ecae8a1466b5cb7ca38e47f20f48d3bb2b47439be5fba6919b90c3429.jpg' is 'http://localhost:8080/c?g=4WFVFQZWgx'.

And clicking the link should get you to the reverse page :

> http://localhost:8080/c?g=4WFVFQZWgx

> This URL is a short link for http://s2.quickmeme.com/img/ed/eddd639ecae8a1466b5cb7ca38e47f20f48d3bb2b47439be5fba6919b90c3429.jpg