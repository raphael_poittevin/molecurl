package fr.poittevin.raphael.molecurl.service.impl;

import fr.poittevin.raphael.molecurl.data.Url;
import fr.poittevin.raphael.molecurl.repository.UrlRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MolecurlServiceImplTest {
    private static final String URL = "www.sometesting.com/a-quite-short-url";
    private static final String URL_2 = "www.sometesting.com/a-little_longer_url";

    @InjectMocks
    private MolecurlServiceImpl molecurlService;

    @Mock
    private UrlRepository urlRepositoryMock;

    @Before
    public void before() {
        when(urlRepositoryMock.save(any(Url.class)))
                .thenAnswer(invocation -> invocation.getArguments()[0]);
    }

    @Test
    public void hashUrl_length_is_less_than_10() throws Exception {
        Url result = molecurlService.generateShortUrl(URL);
        assertEquals(10, result.getShortUrl().length());
    }

    @Test
    public void hashUrl_different_string_produces_different_hash() throws Exception {
        Url result1 = molecurlService.generateShortUrl(URL);
        Url result2 = molecurlService.generateShortUrl(URL_2);
        assertNotEquals(result1.getShortUrl(), result2.getShortUrl());
    }
}