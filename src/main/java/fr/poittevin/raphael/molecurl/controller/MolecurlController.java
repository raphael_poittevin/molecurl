package fr.poittevin.raphael.molecurl.controller;

import fr.poittevin.raphael.molecurl.data.Url;
import fr.poittevin.raphael.molecurl.service.MolecurlService;
import fr.poittevin.raphael.molecurl.service.exception.MolecurlServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/")
public class MolecurlController {
    public static final String URL_PARAMETER_NAME = "url";
    public static final String GOAL_PARAMETER_NAME = "g";

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private MolecurlService service;

    @GetMapping(path = "/encode")
    public String encode() {
        String url = request.getParameter(URL_PARAMETER_NAME);
        Url hash;
        try {
            hash = service.generateShortUrl(url);
        } catch (MolecurlServiceException exception) {
            return String.format("An error occurred while minifying your URL (%s)", exception.getMessage());
        }
        String baseUrl = request.getRequestURL().toString().replace("/encode", "");
        String completeShortUrl = baseUrl
                + "/c?" + GOAL_PARAMETER_NAME + "=" + hash.getShortUrl();
        return "The short version of '<a href=\"" + url + "\">" + url + "</a>' " +
                "is '<a href=\"" + completeShortUrl + "\">" + completeShortUrl + "</a>'.";
    }

    @GetMapping(path = "/c")
    public String decode() {
        String shortUrl = request.getParameter(GOAL_PARAMETER_NAME);
        Url url = service.getUrlFromShortUrl(shortUrl);
        if (url == null) {
            return "This URL doesn't exists";
        }
        return "This URL is a short link for '<a href=\"" + url.getUrl() + "\">" + url.getUrl() + "</a>'";
    }
}