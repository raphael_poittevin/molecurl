package fr.poittevin.raphael.molecurl.repository;

import fr.poittevin.raphael.molecurl.data.Url;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Data Layer used to Create, Read, Update and Delete {@link Url}.
 */
public interface UrlRepository extends MongoRepository<Url, String> {
    /**
     * Retrieves a {@link Url} by the shortUrl associated.
     */
    Url findByShortUrl(String shortUrl);

    /**
     * Retrieves a {@link Url} by the url associated.
     */
    Url findByUrl(String url);
}