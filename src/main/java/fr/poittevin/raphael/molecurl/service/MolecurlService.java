package fr.poittevin.raphael.molecurl.service;

import fr.poittevin.raphael.molecurl.data.Url;
import fr.poittevin.raphael.molecurl.service.exception.MolecurlServiceException;

/**
 * Service responsible for storing URLs as short urls, and retrieving
 * URLs using short values.
 */
public interface MolecurlService {
    /**
     * Will return a stored short URL.
     *
     * @param longUrl the plain long URL to store has a hash.
     * @return an Url containing the original URL and the short one.
     */
    Url generateShortUrl(String longUrl) throws MolecurlServiceException;

    /**
     * Will return the original URL for a given short url.
     *
     * @param shortUrl the short URL.
     * @return an Url containing the original URL and the short one.
     */
    Url getUrlFromShortUrl(String shortUrl);
}
