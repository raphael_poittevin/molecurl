package fr.poittevin.raphael.molecurl.service.impl;

import fr.poittevin.raphael.molecurl.data.Url;
import fr.poittevin.raphael.molecurl.repository.UrlRepository;
import fr.poittevin.raphael.molecurl.service.MolecurlService;
import fr.poittevin.raphael.molecurl.service.exception.MolecurlServiceException;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Implementation of {@link MolecurlService}
 */
@Component
public class MolecurlServiceImpl implements MolecurlService {
    private static final int SHORT_URL_LENGTH = 10;
    private static final int MAX_NB_TRY = 5;

    @Autowired
    private UrlRepository repository;

    @Override
    public Url generateShortUrl(String longUrl) throws MolecurlServiceException {
        Url existingUrl = repository.findByUrl(longUrl);
        if (existingUrl != null) {
            return existingUrl;
        }
        return generateShortUrl(longUrl, 0);
    }

    private Url generateShortUrl(String longUrl, int nbtry) throws MolecurlServiceException {
        if (nbtry >= MAX_NB_TRY) {
            throw new MolecurlServiceException("Unable to generate a unique short URL after 3 tries.");
        }

        String string = RandomStringUtils.randomAlphanumeric(SHORT_URL_LENGTH);

        Url url = repository.findByShortUrl(string);
        if (url != null && !Objects.equals(string, url.getUrl())) {
            return generateShortUrl(longUrl, nbtry + 1);
        }
        url = new Url(string, longUrl);
        return repository.save(url);
    }

    @Override
    public Url getUrlFromShortUrl(String hashedUrl) {
        // TODO add a verification here
        return repository.findByShortUrl(hashedUrl);
    }
}
