package fr.poittevin.raphael.molecurl.service.exception;

import fr.poittevin.raphael.molecurl.service.MolecurlService;

/**
 * Thrown during the execution of the {@link MolecurlService}.
 */
public class MolecurlServiceException extends Exception {

    public MolecurlServiceException() {
        super();
    }

    public MolecurlServiceException(String message) {
        super(message);
    }

    public MolecurlServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
