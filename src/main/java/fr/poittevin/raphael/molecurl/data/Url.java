package fr.poittevin.raphael.molecurl.data;

import org.springframework.data.annotation.Id;

/**
 * Represents an URL and its hashed version.
 */
public class Url {
    @Id
    private String shortUrl;
    private String url;

    public Url() {

    }

    public Url(String shortUrl, String url) {
        this.shortUrl = shortUrl;
        this.url = url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
