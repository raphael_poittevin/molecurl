package fr.poittevin.raphael.molecurl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MolecurlApplication {

	public static void main(String[] args) {
		SpringApplication.run(MolecurlApplication.class, args);
	}
}
